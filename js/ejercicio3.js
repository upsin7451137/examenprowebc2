function generarEdades() {
    const edadesGeneradas = [];
    const categoriasEdad = {
        bebe: 0,
        nino: 0,
        adolescente: 0,
        adulto: 0,
        anciano: 0,
    };

    for (let i = 0; i < 100; i++) {
        const edad = Math.floor(Math.random() * 91); // Generar edades aleatorias de 0 a 90
        edadesGeneradas.push(edad);

        if (edad >= 1 && edad <= 3) {
            categoriasEdad.bebe++;
        } else if (edad >= 4 && edad <= 12) {
            categoriasEdad.nino++;
        } else if (edad >= 13 && edad <= 17) {
            categoriasEdad.adolescente++;
        } else if (edad >= 18 && edad <= 60) {
            categoriasEdad.adulto++;
        } else {
            categoriasEdad.anciano++;
        }
    }

    document.getElementById('edadesGeneradas').textContent = edadesGeneradas.join(', ');
    document.getElementById('bebeCount').textContent = categoriasEdad.bebe;
    document.getElementById('ninoCount').textContent = categoriasEdad.nino;
    document.getElementById('adolescenteCount').textContent = categoriasEdad.adolescente;
    document.getElementById('adultoCount').textContent = categoriasEdad.adulto;
    document.getElementById('ancianoCount').textContent = categoriasEdad.anciano;

    const totalEdades = edadesGeneradas.reduce((a, b) => a + b, 0);
    const edadPromedio = totalEdades / 100;
    document.getElementById('edadPromedio').textContent = edadPromedio.toFixed(2);
}

function limpiarEdades() {
    document.getElementById('edadesGeneradas').textContent = '';
    document.getElementById('bebeCount').textContent = '0';
    document.getElementById('ninoCount').textContent = '0';
    document.getElementById('adolescenteCount').textContent = '0';
    document.getElementById('adultoCount').textContent = '0';
    document.getElementById('ancianoCount').textContent = '0';
    document.getElementById('edadPromedio').textContent = 'N/A';
}
