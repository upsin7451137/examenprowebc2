function calculateConversion() {
    const temperature = parseFloat(document.getElementById('temperature').value);
    const celsiusRadio = document.getElementById('celsius');
    const resultValue = document.getElementById('resultValue');

    if (celsiusRadio.checked) {
        // Conversión de Celsius a Fahrenheit: F = (C * 9/5) + 32
        const result = (temperature * 9/5) + 32;
        resultValue.textContent = result.toFixed(2);
    } else {
        // Conversión de Fahrenheit a Celsius: C = (F - 32) * 5/9
        const result = (temperature - 32) * 5/9;
        resultValue.textContent = result.toFixed(2);
    }

    document.querySelector('.result-section').style.display = 'block';
}

function clearForm() {
    document.getElementById('conversionForm').reset();
    document.querySelector('.result-section').style.display = 'none';
}
